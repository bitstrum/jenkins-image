# Custom Jenkins

This is a base image based on the lts version of jenkins, with automatic install of the [wanted plugins](plugins.txt).

## Test a new Jenkins release

Go to Jenkins, update all plugins and restart Jenkins. Check if no more warnings or version/dependency issues.  
When testing a new version with new plugins, run the [test.sh](test.sh) file and grab the list of plugins via `curl -d "script=$(cat script.groovy)" -v http://localhost:8080/scriptText`

Or via http://localhost:8080/script and execute [the script](script.groovy) in the textfield.
