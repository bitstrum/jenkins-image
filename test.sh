#!/usr/bin/env bash
set -e

IMAGE_NAME="maacarbo/jenkins"

IMAGE_TAG="wip"
JENKINS_VERSION="lts"
PLUGINS_FILE="plugins-latest.txt"
DOCKER_PUSH="false"
while [[ "${1}" ]]; do
  case "${1}" in
    -f )
      PLUGINS_FILE="${2}"
      shift
      ;;
    -p )
      DOCKER_PUSH="true"
      ;;
    -t )
      IMAGE_TAG="${2}"
      shift
      ;;
    -v )
      JENKINS_VERSION="${2}"
      shift
      ;;
  esac

  shift
done

# IMAGE
IMAGE="${IMAGE_NAME}:${IMAGE_TAG}"

# Remove running instance
docker rm -f "${IMAGE}" || true

# Build image
docker build \
  --build-arg jenkins_version=${JENKINS_VERSION} \
  --build-arg plugins_file=${PLUGINS_FILE} \
  -t "${IMAGE}" \
  .

# Push docker image if wanted
if [[ "${DOCKER_PUSH}" == "true" ]]; then
  # Login
  docker login
  docker push "${IMAGE}"
fi

# Run image
docker run \
  --rm \
  -p 8080:8080 \
  -e JAVA_OPTS="-Djenkins.install.runSetupWizard=false" \
  "${IMAGE}"
